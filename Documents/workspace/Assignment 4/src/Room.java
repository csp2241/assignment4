import javax.swing.JOptionPane;

/**
 * 
 * @author csp000us
 *
 *This program allows the user to order a room decoration service from the Cindy's Room Decoration Service Company. 
 *
 *Credit:
 *http://stackoverflow.com/questions/8689122/joptionpane-yes-no-options-confirm-dialog-box-issue-java
 */
public class Room {
	String room;
	String numberRooms;
	String wallcolor;
	String floorType;
	String window;
	
/**
 * This creates the string for the parameters
 */
	public Room() {
		room = "";
		numberRooms = "";
		wallcolor = "";
		floorType = "";
		window = "";
	}
	/**This is the overload constructor
	 * 
	 * @param room this means the selected room.
	 * @param wallcolor this is the selected wallcolor.
	 * @param floorType this is the selected floorType.
	 * @param window
	 */
	public Room(String room, String wallcolor, String floorType, String window) {
		this.room = room;
		this.wallcolor = wallcolor;
		this.floorType = floorType;
			
	}

	/**
	 * This allows the selection of each room
	 */
	public void setnumberRooms() {

	String [] options = {"Room 301", "Room 302", "Room 303", "Room 304"};
			
	String selected;
				
	selected = (String)JOptionPane.showInputDialog(null, "Select your the number rooms ", "Cindy's Room Decoration Services", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
					
	switch(selected) {
			
	case "Room 301":
		JOptionPane.showMessageDialog(null, "you have selected Room 301","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
		break;
	case "Room 302":
		JOptionPane.showMessageDialog(null, "You have selected Room 302","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
		break;
	case "Room 303":
		JOptionPane.showMessageDialog(null, "You have selected Room 303","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
		break;
	case "Room 304":
		JOptionPane.showMessageDialog(null, "You have selected Room 304","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
		break;
	}
		this.numberRooms = selected;
		
	}
	/**
	 * This allows one see the picked rooms
	 * @return
	 */
	public String getnumberRooms() {
		return this.numberRooms;
	}
	
	/**
	 * This allows the user to input the color wished for the room.
	 */
	public void setwallcolor() {
		this. wallcolor = JOptionPane.showInputDialog(null,"Input the color of the wall of the room", "Cindy's Room Decoration Services", JOptionPane.INFORMATION_MESSAGE);
	}
	/**
	 * This is used to set the wall color of the room.
	 * @return 
	 */
	public String getwallcolor() {
		return this.wallcolor;
	}
	/** 
	 * This allows the user to only select out of the choices provided 
	 */
	public void setfloorType() {
	String [] options = {"Hard wood", "Carpet", "Tile"};
	
	String selected;
		
	selected = (String)JOptionPane.showInputDialog(null, "Select your the type of floor you would like in this room ", "Cindy's Room Decoration Services", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			
	switch(selected) {
	
	case "Hard Wood":
		JOptionPane.showMessageDialog(null, "you have selected hardwood","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
	break;
	case "Carpeting":
	JOptionPane.showMessageDialog(null, "You have selected carpeting","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
	break;
	case "Tile":
	JOptionPane.showMessageDialog(null, "You have selected tile","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
	break;
	}
	this.floorType = selected;
	} 
	/**
	 * This prints out the selected floortype of the individual rooms.
	 * @return
	 */
	public String getfloorType() {
		return this.floorType;
	}
	/**
	 * This allows the user to select the number of windows in a room.
	 */
	public void setwindow() {
	String [] options = {"no", "1", "2", "3"};
	
	String selected;
		
	selected = (String)JOptionPane.showInputDialog(null, "Select the number of windows in this room:", "Cindy's Room Decoration Services", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			
			
	switch(selected) {
	
	case "none":
		JOptionPane.showMessageDialog(null, "you have selected no windows","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
	break;
	case "1":
	JOptionPane.showMessageDialog(null, "You have selected 1 window","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
	break;
	case "2":
	JOptionPane.showMessageDialog(null, "You have selected 2 windows","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
	break;
	case "3":
		JOptionPane.showMessageDialog(null, "You have selected 3 windows","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
		break;
	
	}
	this.window = selected;
	} 
	/**
	 * This prints out the selected # window(s) of the individual rooms.
	 * @return
	 */
	public String getwindow(){
		return this.window;
	}
	/**
	 * This allows the user to see the final print out of their selections.
	 */
	public String toString() { 
		return  "Room:" + this.numberRooms +"\n" +
				"Wall Color:" + this.wallcolor + "\n" +
				"Floor Type: " + this.floorType + "\n" + 
				"Number of Window(s):" + this.window + "\n"; 
	}
}