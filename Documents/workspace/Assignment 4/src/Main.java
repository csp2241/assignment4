import javax.swing.JOptionPane;

public class Main {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
			Room cindy = new Room();
		/**
		 * Complete program combined together and it also includes the user's name.
		 */	
			String name;
			name = JOptionPane.showInputDialog(null,"Hello, welcome to Cindy's Room Decoration Services " + "\n" +"To start please input your name:","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);

			final int ATTEMPT = 4;
			int countAttempt = 0;

				Room room1 = new Room();
				Room room2 = new Room();
				Room room3 = new Room();
				Room room4 = new Room();
				
				String [] options = {"1", "2", "3", "4"};
						
				String selected;
							
				selected = (String)JOptionPane.showInputDialog(null, "Select the number of room(s) you would like to decorate: ", "Cindy's Room Decoration Services", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
								
				switch(selected) {
						
				case "1":
					
					while (countAttempt < ATTEMPT) {
					room1.setnumberRooms();
					room1.setwallcolor();
					room1.setfloorType();
					room1.setwindow();
					int dialogButton = JOptionPane.showConfirmDialog(null, "Thank you " + name +" for choosing this Cindy's room renovation company! "+ "\n"+ "These are your preferences for the rooms you have selected: "+ "\n" + "(If they are not select the No button and reselct your options)" + "\n" + room1,"Cindy's Room Decoration Services",JOptionPane.YES_NO_CANCEL_OPTION);
					if(dialogButton == JOptionPane.YES_OPTION) {
						JOptionPane.showMessageDialog(null, "Thank you for you purchase your order is complete","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
						break;
					}else if (dialogButton == JOptionPane.NO_OPTION) {
						countAttempt++;
				     }
					}	
				break;
				case "2":
					
					int answer2 = JOptionPane.NO_OPTION;
					while (countAttempt < ATTEMPT) {
					room1.setnumberRooms();
					room1.setwallcolor();
					room1.setfloorType();
					room1.setwindow();
					room2.setnumberRooms();
					room2.setwallcolor();
					room2.setfloorType();
					room2.setwindow();
					int dialogButton1 = JOptionPane.showConfirmDialog(null, "Thank you " + name +" for choosing this Cindy's room renovation company! "+ "\n"+ "These are your preferences for the rooms you have selected: "+ "\n" + "(If they are not select the No button and reselct your options)" + "\n" + room1 + "\n" + room2,"Cindy's Room Decoration Services",JOptionPane.YES_NO_CANCEL_OPTION);
					if(dialogButton1 == JOptionPane.YES_OPTION) {
						JOptionPane.showMessageDialog(null, "Thank you for you purchase your order is complete","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
						break;
					}else {
						countAttempt++;
				     }
					}
					break;
				case "3":
				
					while (countAttempt < ATTEMPT) {
					room1.setnumberRooms();
					room1.setwallcolor();
					room1.setfloorType();
					room1.setwindow();
					room2.setnumberRooms();
					room2.setwallcolor();
					room2.setfloorType();
					room2.setwindow();
					room3.setnumberRooms();
					room3.setwallcolor();
					room3.setfloorType();
					room3.setwindow();
					int dialogButton = JOptionPane.showConfirmDialog(null, "Thank you " + name +" for choosing this Cindy's room renovation company! "+ "\n"+ "These are your preferences for the rooms you have selected: "+ "\n" + "(If they are not select the No button and reselct your options)" + "\n" + room1 + "\n" + room2 + "\n" + room3,"Cindy's Room Decoration Services",JOptionPane.YES_NO_CANCEL_OPTION);
					if(dialogButton == JOptionPane.YES_OPTION) {
						JOptionPane.showMessageDialog(null, "Thank you for you purchase your order is complete","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
						break;
					}else {
						countAttempt++;
				     }
					}
					break;
				case "4":

					while (countAttempt < ATTEMPT) {
					room1.setnumberRooms();
					room1.setwallcolor();
					room1.setfloorType();
					room1.setwindow();
					room2.setnumberRooms();
					room2.setwallcolor();
					room2.setfloorType();
					room2.setwindow();
					room3.setnumberRooms();
					room3.setwallcolor();
					room3.setfloorType();
					room3.setwindow();
					room4.setnumberRooms();
					room4.setwallcolor();
					room4.setfloorType();
					room4.setwindow();
					int dialogButton = JOptionPane.showConfirmDialog(null, "Thank you " + name +" for choosing this Cindy's room renovation company! "+ "\n"+ "These are your preferences for the rooms you have selected: "+ "\n" + "(If they are not select the No button and reselct your options)" + "\n" + room1 + "\n" + room2+ "\n" + room3+ "\n" + room4,"Cindy's Room Decoration Services",JOptionPane.YES_NO_CANCEL_OPTION);
					if(dialogButton == JOptionPane.YES_OPTION) {
						JOptionPane.showMessageDialog(null, "Thank you for you purchase your order is complete","Cindy's Room Decoration Services",JOptionPane.INFORMATION_MESSAGE);
						break;
					}else {
						countAttempt++;
				     }
					}
					break;
				}
			
		}
}
